from keras.layers import Embedding, Conv1D, Dense, Dropout, Input,\
                         BatchNormalization, Activation, Flatten
from keras.models import Sequential, Model
from keras.optimizers import Adagrad

def vanila_cnn(num_tokens, emb_dim=100, emb_init=None, train_emb=True,
               input_shape=(10,), conv1d_filters=[100], kernel_sizes=[2],
               activations='relu', denses=[1157], init_lr=0.01):
  """Create a simple 1d convolutional neural network and
  return the empty model.

  Arguments:
    num_tokens: Total number of tokens in data (True number)
    emb_dim: Embedding dimension for embedding layer
    input_shape: Shape of each input sample (without the batch size)
    conv1d_filters: List of filter counts for each 1D cnn layer
    activations: Activation to use for all layers
    dense: List of unit counts for each dense layer. 

  Returns:
    vanila_model: Trainable cnn model.
  """
  assert len(denses) > 0 and denses[-1] == 1157,\
         "Last dense layer must exist and have 1157 units."
  assert len(conv1d_filters) == len(kernel_sizes),\
         "Not enough information for convolutional layers."

  # Input tensor
  inputs = Input(shape=input_shape, name="token_input")
  
  # Embedding layer
  if emb_init is not None:
    emb_dim = emb_init.shape[1]
    emb_vects = Embedding(num_tokens+1, emb_dim, 
                          weights=[emb_init], input_length=input_shape[0], 
                          trainable=train_emb)(inputs)
  else:
    emb_vects = Embedding(num_tokens+1, emb_dim, 
                          input_length=input_shape[0])(inputs)

  # Convolutional layer(s)
  if len(conv1d_filters) > 0:
    tmp = Conv1D(conv1d_filters[0], kernel_sizes[0], strides=1, 
                 padding='valid', dilation_rate=1, activation=None, 
                 use_bias=True, kernel_initializer='glorot_uniform', 
                 bias_initializer='zeros', kernel_regularizer=None, 
                 bias_regularizer=None, activity_regularizer=None, 
                 kernel_constraint=None, bias_constraint=None)(emb_vects)
    tmp = BatchNormalization()(tmp)
    tmp = Activation(activations)(tmp)
    for fts, ks in zip(conv1d_filters[1:], kernel_sizes[1:]):
      tmp = Conv1D(fts, ks, strides=1, 
                   padding='valid', dilation_rate=1, activation=None, 
                   use_bias=True, kernel_initializer='glorot_uniform', 
                   bias_initializer='zeros', kernel_regularizer=None, 
                   bias_regularizer=None, activity_regularizer=None, 
                   kernel_constraint=None, bias_constraint=None)(tmp)
      tmp = BatchNormalization()(tmp)
      tmp = Activation(activations)(tmp)
  else:
    tmp = emb_vects

  # Flatten for fc layers
  tmp = Flatten()(tmp)
  
  # Dense layer(s)
  for nunits in denses[:-1]:
    tmp = Dense(nunits, activation=activations)(tmp)
    tmp = Dropout(0.5)(tmp)
  pred = Dense(denses[-1], activation='softmax')(tmp)

  vanila_model = Model(inputs=inputs, outputs=pred)
  optz = Adagrad(lr=init_lr)
  vanila_model.compile(optimizer=optz, loss='sparse_categorical_crossentropy', 
                       metrics=["sparse_categorical_accuracy"])
  
  return vanila_model 
