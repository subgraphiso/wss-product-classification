import numpy as np
from keras import backend as K
from ..data import root_products
from ..models import cnn

"""Experiments:
 - dense: Adjust the number of units and the number of dense layers
"""

def run_expr_dense_units(data=None):
  print("Testing the effect of the number of dense unit layer...")
  for i in [32, 64, 128, 256, 512, 1024, 2048, 4096]:
    _expr_dense_units(data=data, i)

def _expr_dense_units(data=None, num_units=128):
  if data is None:
    train, test, tk, d = root_products.load_data()
  else:
    train, test, tk, d = data
    
  # Init embedding matrix with word vectors
  num_tk = len(tk)
  emb_dim = 100
  init_emb = np.random.uniform(0.05, -0.05, size=(num_tk+1, 100))
  init_emb[0].fill(0)
  for word, vecs in d.items():
    if word in tk:
      idx = tk[word]
      init_emb[idx,:] = vecs 
  
  # Other hyperparams
  batch_size = 128
  e = 20
  init_lr = 0.01 # Adagrad
  
  model1 = cnn.vanila_cnn(num_tk, emb_init=init_emb,
                          conv1d_filters=[1024],
                          kernel_sizes=[1],
                          denses=[num_units, 1157], init_lr=init_lr)
  print(model1.summary()) 
  model1.fit(*train, validation_data=test, batch_size=batch_size, epochs=e, verbose=2)
  val_score = model1.evaluate(*test)
  model1.save("cnn_dense_units_{}_test_{}.keras".format(val_score[1], num_units))
  del model1
  K.clear_session()
