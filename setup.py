from setuptools import setup

setup(name='wss_product_classification',
      version='0.2',
      description='Product classification models',
      url='https://gitlab.com/subgraphiso/wss-product-classification',
      author='WSS Machine Learning Team',
      author_email='ml-team@websosanh.org',
      license='MIT',
      packages=['wss_product_classification',\
                'wss_product_classification.data',\
                'wss_product_classification.experiments',\
                'wss_product_classification.models'],
      zip_safe=False)
