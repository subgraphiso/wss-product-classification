#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 30 15:35:06 2018

@author: syht
"""

def logging(file_path, data):
    f = open(file_path, 'a')
    log_text = []
    for row in data:
        log_text.append('\t'.join(row))
        
    log_text = '\n'.join(log_text)
    f.write(log_text)
    f.write('\n')
    f.close()