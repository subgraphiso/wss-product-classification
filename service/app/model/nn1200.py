import numpy as np
from unicodedata import normalize

class NNModel(object):
    def __init__(self, core, vocab, label_map, reverse_map=None):
        self.core = core
        self.vocab = vocab
        self._label_map = label_map
        self._reverse_map = reverse_map
    def fit(self, X_train, y_train, iterations = 20, X_val = None, y_val = None):
        """NOTE: y contain ClassId, not true CategoryID"""
        X, y = self._preprocess(X_train, y_train)
        if X_val:
            Xv, yv = self._preprocess(X_val, y_val)
        self.core.fit(X_train, y_train , epochs=iterations, batch_size=256,
                      validation_data=(Xv, yv))
    def predict(self, X):
        fX = self._preprocessX(X)
        return self.core.predict_classes(fX)
    def predict_word(self, X):
        if self._reverse_map:
            labels = self.predict(X)
            return [self._reverse_map[l] for l in labels]
    def predict_proba(self, X):
        fX = self._preprocessX(X)
        return self.core.predict(fX)
    def _preprocess(self, X, y):
        X = self._preprocessX(X)
        y = np.array(y)
        return X, y
    def _preprocessX(self, X):
        features = []
        for i in X:
            w = []
            for j in i.split():
                word = normalize('NFC', j.lower())
                if word in self.vocab:
                    w.append(self.vocab[word])
            if len(w) == 0:
                features.append(np.zeros(shape=(300,)))  # Hot fix for words not in vocab
                print("WARNING: Empty feature vector. {}".format(i))
            else:
                w = np.array(w)
                v = np.sum(w, axis=0)
                features.append(v)
        return np.array(features)
