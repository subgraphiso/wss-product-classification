from .ngramRun import *
from .nn1200 import *
from .product_final_model_syht import MyModel
import pickle
import keras
from sklearn.externals import joblib
from time import time

class CombinedModel:

    def __init__(self):
        self._name = "Combined product classification"
        self._sub_models = []
        self.model_count = 0
    
    def add_model(self, model):
        self._sub_models.append(model)
        self.model_count += 1

    def _load_nn(self, nn_file, dict_file):
        nn = keras.models.load_model(nn_file) 
        with open(dict_file, "rb") as f:
            wkvi = pickle.load(f)
        m = NNModel(core=nn, vocab=wkvi, label_map=None, 
                    reverse_map=None)
        self.add_model(m)

    def _load_svc(self, joblib_file):
        clf = joblib.load(joblib_file)
        self.add_model(clf)
    
    def _load_ngram(self, bin_file):
        with open(bin_file, "rb") as f:
            clf = pickle.load(f)
        self.add_model(clf)
    
    def _load_categories(self, cmap_file="../../bin/cmap.pkl",
                         rmap_file="../../bin/rmap/pkl"):
        with open(cmap_file, "rb") as f:
            cmap = pickle.load(f)
        with open(rmap_file, "rb") as f:
            rmap = pickle.load(f)
        self._cmap = cmap 
        self._rmap = rmap
    
    def load_models(self, nn_file="../../bin/nn_1200.keras", 
                    dict_file="../../bin/wiki.vi.pkl",
                    joblib_file="../../bin/product_model_syht-002.joblib",
                    bin_file="../../bin/ngram-model-100k-2.pkl"):
        self._load_nn(nn_file, dict_file)
        self._load_ngram(bin_file)
        self._load_svc(joblib_file)
    
    def to_name(self, p):
        if p in self._rmap:
            return self._rmap[p]
        else:
            return (-1, "unknown")

    def predict(self, X):
        raw_results = []
        results = []
        for m in self._sub_models:
            results.append(m.predict(X))
        for predicts in zip(*raw_results):
            c = Counter((predicts))
            p = c.most_common(1)[0][0]
            if len(c) == self.model_count:
                results.append((p, 0))
            elif len(c) > 1:
                results.append((p, 2))
            else:
                results.append((p, 3))
        return results

    def predict_word(self, X):
        results = self.predict(X)
        word_results = []
        for r, c in results:
            word_results.append((self.to_name(r), r, c))
        return word_results
